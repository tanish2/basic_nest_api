import { Injectable, NotFoundException } from '@nestjs/common';
import { TaskStatus } from './tasks-status.enum';
import { TaskRepository } from './task.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TaskRepository) private task_repository: TaskRepository,
  ) {}

  async getTaskByID(id: string): Promise<Task> {
    const found = await this.task_repository.findOne(id);
    if (!found) {
      throw new NotFoundException(`Task not found id: ${id}`);
    }
    return found;
  }

  async deleteTaskByID(id: string): Promise<void> {
    const delTask = await this.task_repository.delete(id);

    if (delTask.affected == 0) {
      throw new NotFoundException();
    }
  }

  async getAllTasks(): Promise<Task[]> {
    const entities = await this.task_repository.find();
    return entities;
  }

  async createTask(title: string, description: string): Promise<Task> {
    const task = this.task_repository.create({
      title,
      description,
      status: TaskStatus.OPEN,
    });
    return await this.task_repository.save(task);
  }

  async updateTaskByID(id: string, status: string): Promise<void> {
    const updatedTask = await this.task_repository.update(id, {
      status: TaskStatus[status],
    });
    if (updatedTask.affected == 0) {
      throw new NotFoundException();
    }
  }
}
